require 'gem2deb/rake/spectask'

ENV.delete('CI')

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = 'spec/**/*_spec.rb'
  spec.exclude_pattern = 'spec/integration/**/*_spec.rb'
end
